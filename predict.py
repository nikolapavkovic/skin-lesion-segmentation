# import the necessary packages

import matplotlib.pyplot as plt
from skimage.color import rgb2gray
from skimage import data
from skimage.filters import gaussian
from skimage.segmentation import active_contour
from tensorflow.keras.preprocessing.image import img_to_array
from tensorflow.keras.preprocessing.image import load_img
from tensorflow.keras.models import load_model
import numpy as np
import mimetypes
import argparse
import imutils
import cv2
import os


import config

print("Loading object detector")
model = load_model(config.MODEL_PATH)

imagePath = "C:/Users/Nikola/Desktop/skin_lesions/images/ISIC_0029595.jpg"

image = load_img(imagePath, target_size=(224, 224))
image = img_to_array(image) / 255.0
image = np.expand_dims(image, axis=0)

preds = model.predict(image)[0]
(rx, ry, cx, cy) = preds

image = cv2.imread(imagePath)
image = imutils.resize(image, width=600)
(h, w) = image.shape[:2]

rx = int(rx * w)
ry = int(ry * h)
cx = int(cx * w * 1.3)
cy = int(cy * h * 1.3)


img = image

img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

ret, thresh = cv2.threshold(img, 0, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)


s = np.linspace(0, 360, 360)

r = rx + cx*np.cos(np.radians(s))

c = ry + cy*np.sin(np.radians(s))


init = np.array([c, r]).T

snake = active_contour(gaussian(thresh, 3, preserve_range=False), init, alpha=0.009, beta=0.5, gamma=0.0001)
image= cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
fig, ax = plt.subplots(figsize=(7, 7))
ax.imshow(image)
ax.plot(init[:, 1], init[:, 0], '--r', lw=3)
ax.plot(snake[:, 1], snake[:, 0], '-b', lw=3)
ax.set_xticks([]), ax.set_yticks([])
ax.axis([0, image.shape[1], image.shape[0], 0])

plt.show()
